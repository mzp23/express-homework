const express = require('express');
const router = express.Router();
const Fighter = require('../../models/Fighter');
const Fighters = require('../../Fighters');


// Gets All fighters
router.get('/', (req, res) => {
  if (res) {
    Fighter.find().sort({ id: 1 })
      .then(fighters => {
        res.json(fighters)
      })
  } else {
    res.status(400).send("Some error")
  }
});




// get single fighter 

router.get('/:id', (req, res) => {
  Fighter.findOne({ id: req.params.id }, (err) => console.log(err))
    .then(fighter => {
      if (fighter) {
        Fighter.findOne({ id: req.params.id }, (err) => console.log(err))
          .then(fighter => res.json(fighter))
          .then(() => console.log("succes find"))
      } else if (fighter == null) {
        res.status(404).send("User not found");
      } else {
        res.status(400).send("Some error");
      }

    })
    .then(() => console.log("succes find"))
})

router.post('/', (req, res) => {
  const { id, name, health, attack, defense, source } = req.body;
  if (req.body) {
    const newFighter = new Fighter({
      id,
      name,
      health,
      attack,
      defense,
      source
    })
    newFighter.save().then(() => {
      res.json();
    });
  } else {
    res.status(400).send('Some error')
  }
})

// update fighter
router.put('/:id', (req, res) => {
  const { name, attack, defense, health } = req.body
  if (req.body) {
    Fighter.updateOne({ id: req.params.id }, {
      name,
      attack,
      defense,
      health,
    }, (err) => console.log(err))
    .then(() =>console.log(`Update succes`))
    .then(() =>  res.json())
  
  } else {
    res.status(400).send("Some error")
  }

})
// delete fighter
router.delete('/:id', (req, res) => {
  const id = req.params.id;
  Fighter.findOne({ id }, (err) => console.log(err))
    .then((fighter) => {
      if (fighter) {
        fighter.remove()
          .then(() => console.log('delete succes'))
      } else if (fighter == null) {
        res.status(404).send("User not found");
      } else {
        res.status(400).send('Some error')
      }
    })
});

// HARDRESET delete all data from database and add new data from ../../Fighters.js
router.post('/hardreset', (req, res) => {
  if (res) {
    Fighter
      .deleteMany({})
      .then(() => Fighter.create(Fighters))
      .then(() => res.json())
      .then(() => console.log(`Data base was`))
  } else {
    res.status(400).send("Some error")
  }

})
module.exports = router;

