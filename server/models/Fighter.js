const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const FighterSchema = new Schema({
    id: String,
    name: String,
    health : Number,
    attack: Number, 
    defense: Number,
    source: String   
}, { versionKey: false });

module.exports = Fighter = mongoose.model('Fighter', FighterSchema);