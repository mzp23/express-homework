const express = require('express');
const path = require('path');
const logger = require('./middleware/logger');
const members = require('./Fighters');
const mongoose = require('mongoose');
const cors = require('cors');


const app = express();

const db = require('./config/key').mongoURI;

// parse data

// cors
app.use(cors());

// Init middleware
app.use(logger);

// connect to Mongo
mongoose
.connect(db, { useNewUrlParser: true })
.then(() => console.log('MongoDB connected...'))
.catch(err => console.log(err));

// Body Parser Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Set static folder
app.use(express.static(path.join(__dirname, 'public')));

// Members API Routes
app.use('/api/fighters', require('./routes/api/fighters'));


app.use((req, res) => {
  res.status(404).send('404 PAGE NOT FOUND');
});

const PORT = process.env.PORT || 5000;


app.listen(PORT, (err) => {
  if (err) {
      return console.log('Ошибочка вышла', err)
  }
  console.log(`Сервер запущен по адресу http://localhost:${PORT}`)
});