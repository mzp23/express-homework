import { callApi } from '../helpers/apiHelper';


class FighterService {
  async getFighters() {
    try {
      const endpoint = '';
      const apiResult = await callApi(endpoint, 'GET');
      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
       try {
      const endpoint = `${id}`;
      const apiResult = await callApi(endpoint, 'GET');
      return apiResult;
      
    } catch(error){
       throw(error)
    }
 }
// HARDRESET delete all data from database and add new data
  async hardResetData() {
      try {
      const endpoint = `/hardreset`;
      await callApi(endpoint, 'POST');
             
    } catch(error){
       throw(error)
    }
 }


//  update fighter
  async updateFighter(id) {
      try {
      const endpoint = `${id}`;
      const apiResult = await callApi(endpoint, 'PUT');
       return apiResult;
      
    } catch(error){
       throw(error)
    }
 }
    
//  delete fighter
  async deleteFighter(id) {
   
      try {
       
      const endpoint = `${id}`;
      const apiResult = await callApi(endpoint, 'DELETE');
      return apiResult;
      
    } catch(error){
       throw(error)
    }
 }
    
}
export const fighterService = new FighterService();

