import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import {fighteRRR} from './fighter';
import uuidv4 from 'uuid';
class FightersView extends View {
  constructor(fighters) {
    super();
    this.id = 1000;
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
    this.resetData(fighters);
    this.addNewFighter();
    this.openForm();
    this.closeForm();
  }
  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }
  // set info into fightersDetailsMap
   setInfo(fighters) {
       fighters.map(async fighterItem => {
      const fighter = await fighterService.getFighterDetails(fighterItem.id);
      const {id} = fighter;
      this.fightersDetailsMap.set(`${id}`, fighter);
    });
  }
// show modal then clicked on get info button
  showModal(id) {
   const {health, attack, defense, name } = this.fightersDetailsMap.get(id);
   const modal = `
    <div class="modal-close">
        <div class="modal active">
        <svg class="modal__cross " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M23.954 21.03l-9.184-9.095 9.092-9.174-2.832-2.807-9.09 9.179-9.176-9.088-2.81 2.81 9.186 9.105-9.095 9.184 2.81 2.81 9.112-9.192 9.18 9.1z"/></svg>
        <p class="modal__title">
        <span class="name">Name: ${name}</span>
        <form>
        <div class="form-container" >
        <label for="name"></label>
          <input class="form-name" type="string" placeholder="Enter new name" name="name" required>  
          <label for="health">Health: ${health}</label>
          <input class="form-health" type="number" placeholder="Enter new health" name="health" required>
    
          <label for="attack">Attack: ${attack}</label>
          <input class="form-attack" type="number" placeholder="Enter new attack" name="attack" required>

          <label for="defense">Defense: ${defense}</label>
          <input class="form-defense" type="number" placeholder="Enter new defense" name="defense" required>

          <button class="confirm-update" >UPDATE FIGHTER</button>
          </div>
      </form>
      <button class="delete-fighter">DELETE FIGHTER</button>
        </p>    
     </div>
     <div class="overlay active"></div>
     </div>
     `;
    document.body.insertAdjacentHTML('afterbegin', modal);
  }

  // show button then 2 fighters choosed
  showFightButton(){
    const nameElement = this.createElement({ tagName: 'button', className: 'fight' });
    nameElement.innerText = 'START FIGHT';
    document.body.append(nameElement)
  }
  // hide button if 2 fighters not choosed
  hideFightButton(){
    const element = document.querySelector('.fight')
    document.body.removeChild(element);
  }
// hide modal then clicked on X or outside 
 hideModal(){
   let closeButton = document.querySelector('.modal__cross');
   closeButton.addEventListener('click',e =>{
   let element = document.querySelector('.modal-close');
   element.parentElement.removeChild(element);
   });

   let overlay = document.querySelector('.overlay');
   overlay.addEventListener('click',e =>{
    let element = document.querySelector('.modal-close');
    element.parentElement.removeChild(element);
    });
 }


 startFight(){
   const [fighterIdOne, fighterIdTwo] = this.fightersDetailsMap.get('fighters');
   const fighterOne = this.fightersDetailsMap.get(fighterIdOne);
   const fighterTwo = this.fightersDetailsMap.get(fighterIdTwo);
  
//  remove button,checkbox  then game is started
   const fighterReady = document.querySelectorAll(`input:checked`);
   fighterReady.forEach(elem =>{
     elem.previousSibling.remove();
    elem.remove();
   })

  //  disable All checkboxes then game is started
      fighteRRR.fight(fighterOne, fighterTwo, this.fightersDetailsMap);
    document.querySelectorAll(`input[type='checkbox']`).forEach(elem =>{
       elem.disabled = true;
      })
 };
 chooseFighter(){
  const fighters = [];
  const checkedBoxes = document.querySelectorAll('input:checked');
  if(checkedBoxes.length >= 2){
    document.querySelectorAll('input:not(:checked)').forEach(elem=>{
      elem.disabled = true;
    });
  } else {
    document.querySelectorAll('input:not(:checked)').forEach(elem=>{
      elem.disabled = false;
    });
  }
  if(checkedBoxes.length >= 2 ){
    checkedBoxes.forEach(element => {
      let id = element.parentElement.getAttribute('data-id')
      fighters.push(id);
    });
    this.fightersDetailsMap.set('fighters', fighters)
    // show button then 2 fighters choosed
    this.showFightButton()
    document.querySelector('.fight').addEventListener('click', e => {
      this.startFight()
      // after start fight change properties of button
      document.querySelector('.fight').innerText = 'START NEW GAME';
      document.querySelector('.fight').classList.add('new-game');
      // reload page then clicked on START NEW GAME button
      if(e.target.classList.contains('new-game')){
       e.target.addEventListener('click', () => location.reload())
      }
    });
  } 
  // hide button if 2 fighters not choosed
  else if(checkedBoxes.length < 2 && document.querySelector('.fight')){
    this.hideFightButton();
  }
}
// delete fighter from db
deleteFighter(id){
  const fighter = this.fightersDetailsMap.get(id);
  const button = document.querySelector('.delete-fighter');
  if(button){
    button.addEventListener('click', async (e) => {
         await fighterService.deleteFighter(fighter.id).then(location.reload())
})}}
 // reset data to initial state
resetData(){
   const button = document.querySelector('.reset-data');
   button.addEventListener('click', async () => {
     await fighterService.hardResetData().then( location.reload());
})}

// add new fighter to db
// open form to add new fighter
openForm() { document.querySelector(".open-button").addEventListener('click', ()=>{
  document.getElementById("myForm").style.display = "block";
  document.querySelector('.open-button').style.display = "none";
})}
// close form
closeForm() { document.querySelector(".cancel").addEventListener('click', ()=>{
    document.getElementById("myForm").style.display = "none";
    document.querySelector('.open-button').style.display = "block";
  })}

addNewFighter(){
  //  TO DO
  const id = uuidv4();

  const addFighter = document.querySelector('.add-fighter');
  addFighter.addEventListener('click',  async (e) => { 
    e.preventDefault();
      let reqObj = {
        id,
        name: document.querySelector("input[name='name']").value,
        health:  document.querySelector("input[name='health']").value,
        attack:  document.querySelector("input[name='attack']").value,
        defense: document.querySelector("input[name='defense']").value,
        source: document.querySelector("input[name='source']").value
      }
      if(reqObj.name === "" || reqObj.health === "" ||reqObj.attack === "" ||reqObj.defense === "" ||reqObj.source === ""){
        alert('Fill all fields')
      } else {
   try {await fetch('http://localhost:5000/api/fighters/', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(reqObj)
    }).then(location.reload(  ));}catch(err) {
      throw(err)}}})
      
}
// update fighter in db
updateFighter(id){
  const fighter = this.fightersDetailsMap.get(id);

  const addFighter = document.querySelector('.confirm-update');
  addFighter.addEventListener('click',  async (e) => { 
    e.preventDefault();
 
      let reqObj = {
        name: document.querySelector("input[name='name']").value,
        health:  document.querySelector("input[name='health']").value,
        attack:  document.querySelector("input[name='attack']").value,
        defense: document.querySelector("input[name='defense']").value,
      }
      if(reqObj.name === "" || reqObj.health === "" ||reqObj.attack === "" ||reqObj.defense === ""){
        alert('Fill all fields')
      }else {
        try {
          await fetch(`http://localhost:5000/api/fighters/${fighter.id}`, {
          method: 'PUT',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(reqObj)
        }).then(location.reload(  ));}catch(err) {
          throw(err)
        }
      }
    
      })
      
}
   handleFighterClick(event, fighter) { 
     let element = event.target;
     const id = fighter.id;
     if(element.classList.contains("fighter-info")){
       this.showModal(id);
       this.hideModal();
       
       
      }
      if (element.classList.contains("fighter-checkbox")) {
        this.chooseFighter();
      }
      //  delete fighter from database          
      this.deleteFighter(id);
     
      // update fighter
      this.updateFighter(id);
    
   
  }
  
}

export default FightersView;