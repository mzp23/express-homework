class Fighter {
 constructor(){
   
 }
 hit(one, two, map){
   let damage = this.getHitPower(one);
   let resistance = this.getBlockPower(two);
   let result = damage - resistance;
   let health = map.get(two.id).health;
   console.log(`${one.name} deal ${damage.toFixed(2)} damage but ${two.name} blocked ${resistance.toFixed(2)} damage. ${two.name} get ${result > 0 ? result.toFixed(2) :'miss '} damage.`);
    return result > 0 ? map.get(two.id).health = health - result : health; 
   
 }
 fight(one, two, map){
        while(one.health > 0 || two.health > 0){
         this.hit(one,two,map)
                 if(two.health <= 0){
                    console.log(`${one.name} WIN`);
                 two.health = 0;
              break;
          }
          this.hit(two,one, map)
          if(one.health <= 0){
            console.log(`${two.name} WIN`);
              one.health = 0;
              break;
          }
      }
 }
//  calculate hit damage
getHitPower(fighter){
    const criticalHitChance = (Math.random() * 1) +1 ;
    const power = fighter.attack * criticalHitChance;
    return power;
 }
//  calculate impact protection
 getBlockPower(fighter){
    const dodgeChance = (Math.random() * 1) +1  ;
    const power = fighter.defense * dodgeChance
    return power;
 }
  
}
 export const fighteRRR = new Fighter();
