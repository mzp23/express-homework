import View from './view';

class FighterView extends View {
  constructor(fighter, handleClick) {
    super();

    this.createFighter(fighter, handleClick);
  }

  createFighter(fighter, handleClick) {
    const { name, source, id } = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);
    const infoButton = this.createButton();
    const checkbox = this.createCheckbox();
    this.element = this.createElement({ tagName: 'div', className: 'fighter' });
    this.element.dataset.id = id;
    this.element.append(imageElement, nameElement,infoButton,checkbox );
    this.element.addEventListener('click', event => handleClick(event, fighter), true);
  }
  createName(name) {
    const nameElement = this.createElement({ tagName: 'span', className: 'name' });
    nameElement.innerText = name;
    return nameElement;
  }
  createImage(source) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });
    return imgElement;
  }
  // button for get info of each fighter
  createButton(){
    const nameElement = this.createElement({ tagName: 'button', className: 'fighter-info' });
    nameElement.innerText = 'get info';
    return nameElement;
  }
// checkbox for choose fighter
  createCheckbox(){
    const nameElement = this.createElement({ tagName: 'input', className: 'fighter-checkbox' });
    nameElement.type = 'checkbox';
    return nameElement
  }
  

 }
export default FighterView;