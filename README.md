# Expess home work

### Demo
[Fighter game](https://mzp23figter.herokuapp.com/) - Hosted on heroku.

## Front Instalation

`npm install`

`npm run build`

`npm run start`

open http://localhost:8080/

## Server Instalation
`npm install`
### Serve on localhost:5000
`npm run dev`


* GET: http://localhost:5000/api/fighters/
   получение массива всех бойцов

* GET: http://localhost:5000/api/fighters/:id
  получение одного бойца по ID

* POST: http://localhost:5000/api/fighters/
  создание бойца по данным передаваемым в теле запроса

* PUT: http://localhost:5000/api/fighters/:id
  обновление бойца по данным передаваемым в теле запроса

* DELETE: http://localhost:5000/api/fighters/:id
  удаление одного бойца по ID